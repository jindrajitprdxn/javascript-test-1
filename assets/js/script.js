/* Add click event*/
document.querySelector('.test-ip').addEventListener("click", (e) => {
	e.preventDefault()
	if(document.querySelector('.span-text')) {
		document.querySelector('.span-text').remove();
	}
	let ipText = document.querySelector('.ip-address').value;
	var error = document.createElement('span');
	if(ipText === "") {
		/* required field validation*/
		error.classList.add("error");
		error.classList.add("span-text");
		error.innerText = 'This field is Manditory';
	} else if(!ipText.match('/[0-9.]/g')) {
		/* check enter text is valid or not by using jsx*/
			error.classList.add("result");
			error.classList.add("span-text");
			error.innerText = 'False';
	} else {
		let values = ipText.split('.')
		if(values.length < 4 || values.length > 4) {
			error.classList.add("result");
			error.classList.add("span-text");
			error.innerText = 'False';
		} else {
			let count = 0
			values.map(value => {
				if(value === "" || value > 255 || value < 0) {
					count++
					error.innerText = 'False';
					error.classList.add("result");
					error.classList.add("span-text");
				} else {
					if(count === 0) {
						error.innerText = 'True';
						error.classList.add("result");
						error.classList.add("span-text");
					}
				}
			})
		}
	}
		document.querySelector('form').appendChild(error)
})